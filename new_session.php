<?php
  require_once 'db/common.php';
?>
<html>
  <head>
    <?php htmlHead(); ?>
    <title>Dilemmio</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align:center;">Create a session</h2>
      <form action="create_session.php" method="get">
        <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">Your Name</label>
          <div class="col-sm-10">
            <input type="text" name="name" class="form-control" id="inputName" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputSessName" class="col-sm-2 col-form-label">Session Name</label>
          <div class="col-sm-10">
            <input type="text" name="sessionname" class="form-control" id="inputSessName" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPass" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
            <input type="text" name="pass" class="form-control" id="inputPass">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2"></div>
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>