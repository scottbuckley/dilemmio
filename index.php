<?php
  require_once "db/common.php";

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // make sure we have a valid session
  $session = getSession($sessionid, $pass);
  if ($session == false) {
    require "no_session.php";
    exit();
  }
  dieIfInvalidSessionOrPlayer($session, $playerid);

  // if session isn't started, show waiting room
  if ($session['phase'] == PHASE_NOTSTARTED) {
    require "waiting_room.php";
    exit();
  }

  // if the session is started, show the game state
  require "game.php";

?>