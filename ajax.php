<?php
  require_once 'db/common.php';

  // response will always be json
  header('Content-Type: application/json');

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];
  $request = $_REQUEST['req'];
  
  if ($request == 'waiting') {
    // waiting room response. this is either the current list of players,
    // or a player state if the phase has changed away from PHASE_NOTSTARTED
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);

    if (isset($session['phase']) && $session['phase'] == PHASE_NOTSTARTED) {
      // session is still in waiting mode. return player list.
      $playerList = getPlayerNamesInSession($sessionid);
      echo(json_encode(array("response"=>"players","players"=>$playerList)));
    } else {
      // session has progressed. send session data.
      echo(json_encode(array("response"=>"session", "session"=>$session)));
    }
  } else if ($request == 'herochoice') {
    // hero's choice. first make sure that it's the hero making this request,
    // and the session is in the right state.
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
    if ($session['hero'] != $playerid) error('only the hero can do this.');
    if ($session['state'] != STATE_HERO_CHOOSING) error("wrong session state.");

    // get the choice
    $choice  = $_REQUEST['choice'];
    if (!($choice == "A" || $choice == "B"))
      error("invalid choice");

    // make the choice
    if (makeHeroChoice($session, $choice)) {
      echo(json_encode(array("response"=>"choice_made")));
    } else {
      error("something went wrong while recording your choice.");
    }
  } else if ($request == 'tricksterchoice') {
    // trickster's choice. first make sure that it's the trickster making this request,
    // and the session is in the right state.
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
    if ($session['trickster'] != $playerid) error('only the hero can do this.');
    if ($session['state'] != STATE_TRICKSTER_CHOOSING) error("wrong session state.");

    // get the choice
    $choice  = $_REQUEST['choice'];

    // make the choice
    if (makeTricksterChoice($session, $choice)) {
      echo(json_encode(array("response"=>"choice_made")));
    } else {
      error("something went wrong while recording your choice.");
    }
  } else if ($request == 'heroresponse') {
    // hero's choice. first make sure that it's the hero making this request,
    // and the session is in the right state.
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
    if ($session['hero'] != $playerid) error('only the hero can do this.');
    if ($session['state'] != STATE_HERO_RESPONDING) error("wrong session state.");

    // get the choice
    $choice  = $_REQUEST['choice'];
    if (!($choice == "A" || $choice == "B"))
      error("invalid choice");

    // make the choice
    if (makeHeroResponse($session, $choice)) {
      echo(json_encode(array("response"=>"choice_made")));
    } else {
      error("something went wrong while recording your choice.");
    }
  } else if ($request == 'sessionchanged') {
    // checking to see if the session data has changed in any way since some point
    // or a player state if the phase has changed away from PHASE_NOTSTARTED
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
    if ($session['chcount'] == $_REQUEST['chc']) {
      echo(json_encode(array("response"=>"same")));
    } else {
      echo(json_encode(array("response"=>"different")));
    }
  } else if ($request == 'debug_setstate') {
    /// FOR DEBUG ONLY
    $session = getSession($sessionid, $pass);
    dieIfInvalidSessionOrPlayer($session, $playerid, true);
    $db = dbConnect();
    $q=$db->prepare('
      UPDATE sessions
      SET state = ?
      WHERE id = ?
    ');
    $q->execute(array($_REQUEST['state'], $session['id']));
    $db = null;
    echo "done";
    return true;
  } else {
    echo json_encode(array("response"=>"invalid request type."));
  }
?>