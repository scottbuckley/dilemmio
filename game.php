<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get the session, die if it's invalid
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);

  // get some session data
  $players = getPlayersInSession($sessionid);
  $playername = $players[$playerid];
  $sessionname = $session['name'];
  $sessionchc = $session['chcount'];
  $state = $session['state'];

  // hero details
  $heroid = $session['hero'];
  $heroname = $players[$heroid];
  $heroisme = ($heroid == $playerid);

  //trickster details
  $tricksterid = $session['trickster'];
  $trickstername = $players[$tricksterid];
  $tricksterisme = ($tricksterid == $playerid);

  // dilemma details
  $dilemmaid = $session['dilemma'];
  $dilemma = getDilemma($dilemmaid);

  // page state
  $herochoosing = ($heroisme && $state == STATE_HERO_CHOOSING);
  $heroresponding = ($heroisme && $state == STATE_HERO_RESPONDING);
  $tricksterchoosing = ($tricksterisme && $state == STATE_TRICKSTER_CHOOSING);
  $refreshonchange = !$herochoosing && !$tricksterchoosing && !$heroresponding;




  $showScores = function() use ($session, $players) {
    $scores = getPlayerScores($session['id']);
    echo '<div class="scores text-secondary">';
    foreach ($players as $playerid => $playername) {
      $score = $scores[$playerid];
      if (!$score) $score = 0;
      echo "<span><b>$playername</b>: $score</span>";
      // echo $playername;
      // echo $scores[$playerid];
    }
    // print_r($scores);
    echo '</div>';
  };


  $optionChangeButtons = function() use ($dilemma, $session) {
    echo   "<button id=\"optionAbutton\" type=\"button\" class=\"btn btn-primary btn-block btn-lg\">$dilemma[optionA]</button>";
    echo   "<button id=\"optionBbutton\" type=\"button\" class=\"btn btn-primary btn-block btn-lg\">$dilemma[optionB]</button>";
  };

  // render the dilemma, including the options, and optionally with buttons to
  // make a choice.
  $showDilemma = function($optiondesc, $chosen=false, $choosable=false) use ($session, $dilemma, $heroname, $heroisme, $optionChangeButtons) {
    if ($heroisme) echo '<p><b>You</b> are the hero, facing the following dilemma:</p>';
    else           echo "<p><b>$heroname</b> is the hero, facing the following dilemma:</p>";
    echo '<div class="card">';
    echo "  <h3 class=\"card-header\" style=\"text-align:center;\">$dilemma[title]</h3>";
    echo "  <div class=\"card-body\">$dilemma[description]</div>";
    // echo '<div class="dilemma-container">';
    // echo "  <h3>$dilemma[title]</h3>";
    // echo "  <p>$dilemma[description]</p>";
    // echo "  <p>$optiondesc</p>";
    echo '<div class="card-footer">';
    echo "<p>$optiondesc</p>";
    if ($choosable == false) {
      $optionAclass = $optionBclass = 'btn-outline-dark';
      if ($chosen && $session['herochoice'] == CHOICE_OPTIONA) $optionAclass = 'btn-dark';
      if ($chosen && $session['herochoice'] == CHOICE_OPTIONB) $optionBclass = 'btn-dark';
      echo   "<button type=\"button\" class=\"btn btn-block btn-lg $optionAclass\" disabled>$dilemma[optionA]</button>";
      echo   "<button type=\"button\" class=\"btn btn-block btn-lg $optionBclass\" disabled>$dilemma[optionB]</button>";
    } else {
      $optionChangeButtons();
    }
    echo '</div></div>';
  };

  // render a single card, optionally with a 'choose'' button
  $showCard = function($card, $choosable=false) { ?>
      <div class="card deceitcard">
        <h5 class="card-header" style="text-align:center;"><?=$card['title']?></h5>
        <div class="card-body">
          <p class="card-text"><?=$card['description']?></p>
        </div>
        <?php if($choosable) { ?>
        <div class="card-footer">
          <button data-cardid="<?=$card['id']?>" class="btn btn-lg btn-primary chooseButton">Play card</button>
        </div>
        <? } ?>
      </div>
  <?php };

  // render the player's whole hand
  $showCards = function($choosable=false) use ($sessionid, $playerid, $showCard) {
    // if ($title) echo '<h3>Your cards:</h3>';
    echo '<h3 style="text-align:center; margin-top:3ex;">Your cards:</h3>';
    echo '<div class="card-container">';
    $cards = getPlayerHand($sessionid, $playerid);
    foreach ($cards as $card)
      $showCard($card, $choosable);
    echo '</div>';
  };

  // give dilemma buttons their behaviour
  $dilemmaButtonBehaviour = function() use ($state) { 
    if ($state == STATE_HERO_CHOOSING)   $ajaxreq = 'herochoice';
    if ($state == STATE_HERO_RESPONDING) $ajaxreq = 'heroresponse';
    ?>
    function chooseOptionFunc(AorB) {
      return function() {
        $.getJSON(`ajax.php?req=<?=$ajaxreq?>&choice=${AorB}&<?=$_SERVER['QUERY_STRING']?>`, function(data) {
          // at this point, the choice has been made. refresh the page.
          console.log(data);
          if (data['response'] === 'choice_made') {
            console.log('choice made. reloading.');
            location.reload();
          } else {
            alert(data);
          }
        });
      };
    }
    $("#optionAbutton").off('click').click(chooseOptionFunc("A"));
    $("#optionBbutton").off('click').click(chooseOptionFunc("B"));
  <?php };

  // give deceit buttons their behaviour
  $cardButtonBehaviour = function() {?>
    $('.chooseButton').off('click').click(function(){
      var cardid = this.dataset.cardid;
      $.getJSON(`ajax.php?req=tricksterchoice&choice=${cardid}&<?=$_SERVER['QUERY_STRING']?>`, function(data) {
        console.log(data);
        if (data['response'] === 'choice_made') {
          console.log('choice made. reloading.');
          location.reload();
        } else {
          alert(data);
        }
      });
    });
  <?php };

  $refreshBehaviour = function() use ($sessionchc) {?>
    setInterval(function() {
      $.getJSON('ajax.php?req=sessionchanged&chc=<?=$sessionchc?>&<?=$_SERVER['QUERY_STRING']?>', function(data){
        console.log(data);
        if (data['response'] === 'different')
          location.reload();
      });
    }, <?=PING_INTERVAL?>);
  <?php };
  


?>

<html>
  <head>
    <?php htmlHead(); ?>
    <title>Dilemmio</title>
  </head>
  <body>
    <?php $showScores(); ?>
    <div class="container">
<?php
    // echo "<p>You are <b>$playername.</b></p>";

    // show the scores
    

    // show the dilemma
    if ($state == STATE_HERO_CHOOSING) {
      if ($heroisme) {
        $showDilemma('You must choose one of the following options:', false, true);
        // $showDilemma('Do you:', false, true);
      } else {
        $showDilemma("<b>$heroname</b> must choose one of the following options:");
        // echo "<p>After $heroname has chosen, you will try to change their mind using one of the following cards:</p>";
        $showCards();
      }
    } else if ($state == STATE_TRICKSTER_CHOOSING) {
      if ($tricksterisme) {
        $showDilemma("$heroname has chosen the following option:", true);
        echo "<p><b>You</b> must now attempt to change $heroname's mind.</p>";
        //  must now modify the dilemma in an attempt to change $heroname's mind, using one of the following deceit cards:</p>";
        $showCards(true);
      } else if ($heroisme) {
        $showDilemma('You have chosen the following option:', true);
        echo "<p><b>$trickstername</b> is now the trickster.</p>";
      } else {
        $showDilemma("$heroname has chosen the following option:", true);
        echo "<p><b>$trickstername</b> is now the trickster.</p>";
        $showCards();     
      }
    } else if ($state == STATE_HERO_RESPONDING) {
      if ($heroisme) {
        $showDilemma('You have chosen the following option:', true);
        echo "<p><b>$trickstername</b> has played the following deceit card.</p>";
        $showCard(getCard($session['deceit']));
        echo "<p><b>You</b> must again choose from one of the following options:</p>";
        $optionChangeButtons();
      } else if ($tricksterisme) {
        $showDilemma("$heroname has chosen the following option:", true);
        echo "<p><b>You</b> have played the following deceit card.</p>";
        $showCard(getCard($session['deceit']));
        echo "<p><b>$heroname</b> must now make their choice again.</p>";
        // echo "<p>Explain to $heroname how this card changes the dilemma. Your goal is to convince them
                //  to change their mind.</p>";
        // echo "<p>$heroname must then decide which option they will choose, given your changes to the dilemma.</p>";
      } else {
        $showDilemma("$heroname has chosen the following option:", true);
        echo "<p>$trickstername has played the following deceit card.</p>";
        $showCard(getCard($session['deceit']));
        echo "<p>$heroname must now make their choice again.</p>";
        $showCards();
      }
    }
?>
    </div>
    <script type="text/javascript">
      // jquery 'onload'
      $(function() { <?php
        
        // only one of the following should happen.
        // either gives behaviour to buttons on the page,
        // or sets up a regular ping to know when to refresh the page

        if ($herochoosing || $heroresponding)
          $dilemmaButtonBehaviour();

        if ($tricksterchoosing)
          $cardButtonBehaviour();

        if ($refreshonchange)
          $refreshBehaviour();
        
      ?> });
    </script>
  </body>
</html>