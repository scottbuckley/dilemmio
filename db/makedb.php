<?php
  require_once "common.php";

  function dbCreateTables() {
    $db = dbConnect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // dilemma cards
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS dilemmas (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            title            TEXT NOT NULL,
            description      TEXT NOT NULL,
            optionA          TEXT NOT NULL,
            optionB          TEXT NOT NULL
        );');
    $q->execute();

    // deceit cards
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS deceits (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            title            TEXT NOT NULL,
            description      TEXT NOT NULL
        );');
    $q->execute();

    // game sessions
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS sessions (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            name             TEXT NOT NULL,
            password         TEXT,
            adminplayer      INTEGER NOT NULL,

            phase            INTEGER DEFAULT 0,
            state            INTEGER DEFAULT 0,
            round            INTEGER DEFAULT 0,

            hero             INTEGER DEFAULT -1,
            dilemma          INTEGER DEFAULT -1,
            herochoice       INTEGER DEFAULT 0,

            trickster        INTEGER DEFAULT -1,
            deceit           INTEGER DEFAULT -1,

            chcount          INTEGER DEFAULT 0,
            lastupdate       DATETIME DEFAULT CURRENT_TIMESTAMP
        );');
    $q->execute();

    // players
    $q = $db->prepare('
        CREATE TABLE IF NOT EXISTS players (
            id               INTEGER PRIMARY KEY AUTOINCREMENT,
            session          INTEGER,
            name             TEXT NOT NULL
            /* FOREIGN KEY(session) REFERENCES sessions(id) */
        );');
    $q->execute();

    // dealt deceit cards
    $q = $db->prepare("
        CREATE TABLE IF NOT EXISTS dealtcards (
            session          INTEGER,
            placement        INTEGER DEFAULT ".CARD_UNDEALT.",
            player           INTEGER,
            card             INTEGER
        );");
    $q->execute();

    // session history
    $q = $db->prepare("
        CREATE TABLE IF NOT EXISTS history (
            session          INTEGER,
            round            INTEGER,
            hero             INTEGER,
            dilemma          INTEGER,
            trickster        INTEGER,
            deceit           INTEGER,
            successful       BOOLEAN
        );");
    $q->execute();


    // a trigger to keep track of changes to the 'sessions' table
    $q = $db->prepare("
        CREATE TRIGGER 'trackChanges'
        AFTER UPDATE ON sessions
        FOR EACH ROW
        BEGIN
            UPDATE sessions
            SET lastupdate=CURRENT_TIMESTAMP,
                chcount=OLD.chcount+1
            WHERE id = OLD.id;
        END
        );");
    $q->execute();




    

    $db = null;
  }


  dbCreateTables();

?>