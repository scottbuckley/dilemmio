<?php
  require "common.php";
  $title = $_REQUEST['title'];
  $desc = $_REQUEST['desc'];
  $optionA = $_REQUEST['optionA'];
  $optionB = $_REQUEST['optionB'];
  
  $db = dbConnect();
  $q=$db->prepare("
    INSERT INTO dilemmas
    (title, description, optionA, optionB)
    VALUES (?, ?, ?, ?)
  ;");
  $q->execute(array($title, $desc, $optionA, $optionB));
  $db = null;
?>
<html>
   <head>
      <title>Dilemma created</title>
      <meta http-equiv = "refresh" content = "0; url = new_dilemma.html" />
   </head>
   <body>
      <p>Saved. Redirecting...</p>
   </body>
</html>