<?php

  ini_set('display_errors', 1);

  define("PHASE_NOTSTARTED", 0);
  define("PHASE_PLAYING", 1);

  define("STATE_HERO_CHOOSING", 0);
  define("STATE_TRICKSTER_CHOOSING", 1);
  define("STATE_HERO_RESPONDING", 2);
  /*
    HERO_CHOOSING -> TRICKSTER_CHOOSING -> HERO_RESPONDING ->
    TRICKSTER_CHOOSING -> HERO_RESPONDING ...
  */

  define("CARD_UNDEALT", 0); // not sure this one will be used
  define("CARD_INHAND", 1);
  define("CARD_PLAYED", 2);
  define("CARD_DISCARDED", 3); // not sure this one will be used either

  define("CHOICE_UNCHOSEN", 0);
  define("CHOICE_OPTIONA", 1);
  define("CHOICE_OPTIONB", 2);

  define("PING_INTERVAL", 2500);
  define("HAND_SIZE", 5);

  function htmlHead() {?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="commonstyle.css">
  <?php }

  // path to the database file.
  define('DB_PATH', __DIR__ . '/dilemmio.db');

  // get a connection to the database
  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
          return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }


  // get a session's details, if it exists
  function getSession($sessid, $pass="") {
    // numeric id
    if (!is_numeric($sessid)) return false;

    // get data
    $db = dbConnect();
    $q=$db->prepare("
      SELECT * FROM sessions
      WHERE id = ?
    ");
    $q->execute(array($sessid));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;

    // no response
    if (!$res) return false;

    // wrong password
    if ($res['password'] != $pass) return "wrong_password";

    // all good
    unset($res['password']);
    return $res;    
  }


  // handling errors
  function error($text, $json=false) {
    if ($json) {
      echo json_encode(array("error"=>$text));
      exit();
    } else {
      die($text);
    }
  }

  function dieIfInvalidSession($session, $json=false) {
    if ($session == false) {
      error('session not found', $json);
    } else if ($session == 'wrong_password') {
      error('wrong password', $json);
    }
  }

  function dieIfInvalidSessionOrPlayer($session, $playerid, $json=false) {
    dieIfInvalidSession($session, $json);
    if (!checkPlayerInSession($playerid, $session['id']))
    error('player not in session', $json);
  }

  function dieIfNotAdmin($session, $playerid, $json=false) {
    if (!isAdmin($session, $playerid))
      error("only the admin player can do this", $json);
  }

  function isSessionResponse($sess) {
    if ($sess == false) return true;
    if ($sess == "wrong_password") return true;
    if (isset($sess['id']) && isset($sess['phase'])) return true;
    return false;
  }

  function isAdmin($session, $playerid) {
    if (isset($session['adminplayer']) && $session['adminplayer'] == $playerid)
      return true;
    return false;
  }

  function getWaitingSessions() {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT id, name FROM sessions
      WHERE phase = ?
    ");
    $q->execute(array(PHASE_NOTSTARTED));
    $res = $q->fetchAll(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function createPlayer($name, $session=null) {
    $db = dbConnect();
    $q=$db->prepare("
      INSERT INTO players
      (name, session) VALUES (?, ?)
    ");
    $q->execute(array($name, $session));
    $id = $db->lastInsertId();
    $db = null;
    return $id;
  }

  function createSession($name, $playerid, $pass) {
    $db = dbConnect();
    $q=$db->prepare("
      INSERT INTO sessions
      (name, adminplayer, password) VALUES (?, ?, ?)
    ");
    $q->execute(array($name, $playerid, $pass));
    $id = $db->lastInsertId();
    $db = null;
    return $id;
  }

  function addPlayerToSession($playerid, $sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE players
      SET session = ?
      WHERE id = ?
    ");
    $q->execute(array($sessionid, $playerid));
    $db = null;
    return true;
  }

  function checkPlayerInSession($playerid, $sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT 1 FROM players
      WHERE id = ? AND session = ?
    ");
    $q->execute(array($playerid, $sessionid));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;

    if ($res) return true;
    return false;
  }

  // return all the player names in a session. it's assumed
  // that this is in a consistent order (right now just using
  // table order - can change later if needed)
  function getPlayerNamesInSession($sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT name FROM players
      WHERE session = ?
    ");
    $q->execute(array($sessionid));
    $res = $q->fetchAll(PDO::FETCH_COLUMN);
    $db = null;
    return $res;
  }

  // same as getPlayerNamesInSession, but returning
  // player ids as well as names
  function getPlayersInSession($sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT id, name FROM players
      WHERE session = ?
    ");
    $q->execute(array($sessionid));
    $res = $q->fetchAll(PDO::FETCH_KEY_PAIR);
    $db = null;
    return $res;
  }


  function getWaitingRoomUpdate($sessionid, $pass) {
    $session = getSession($sessionid, $pass);
    if (isset($session['phase']) && $session['phase'] == PHASE_NOTSTARTED)
      return getPlayerNamesInSession($sessionid);
    return $session;
  }

  function getRandomPlayerIDs($sessionid, $count=1) {
    $players = getPlayersInSession($sessionid);
    return array_rand($players, $count);
  }

  function startGame($sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE sessions
      SET phase = ?
      WHERE id = ?
    ");
    $q->execute(array(PHASE_PLAYING, $sessionid));
    $db = null;
  }

  function setHero($sessionid, $playerid) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE sessions
      SET hero = ?
      WHERE id = ?
    ");
    $q->execute(array($playerid, $sessionid));
    $db = null;
  }

  function setTrickster($sessionid, $playerid) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE sessions
      SET trickster = ?
      WHERE id = ?
    ");
    $q->execute(array($playerid, $sessionid));
    $db = null;
  }

  function setDilemma($sessionid, $dilemma) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE sessions
      SET dilemma = ?
      WHERE id = ?
    ");
    $q->execute(array($dilemma, $sessionid));
    $db = null;
  }

  function getRandomDilemma() {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT id
      FROM dilemmas
      ORDER BY RANDOM()
      LIMIT 1
    ");
    $q->execute();
    $res = $q->fetch(PDO::FETCH_COLUMN);
    $db = null;
    return $res;
  }


  function getDilemma($dilemmaid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT *
      FROM dilemmas
      WHERE id = ?
    ");
    $q->execute(array($dilemmaid));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function getPlayerScores($sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT trickster, count(*) FROM history
      WHERE successful AND session=?
      GROUP BY trickster
    ");
    $q->execute(array($sessionid));
    $res = $q->fetchAll(PDO::FETCH_KEY_PAIR);
    $db = null;
    return $res;
  }

  // function getUndealtCards($sessionid, $count) {
  //   $db = dbConnect();
  //   // select all deceit card ids, where that id doesn't exist in the
  //   // 'dealtcards' database under this sessionid. this represents cards
  //   // that haven't been dealt for this session.
  //   $q=$db->prepare("
  //     SELECT deceits.id
  //     FROM deceits
  //     LEFT JOIN dealtcards ON dealtcards.session = ? AND dealtcards.card = deceits.id
  //     WHERE dealtcards.card IS NULL
  //     ORDER BY RANDOM()
  //     LIMIT ?
  //   ");
  //   $q->execute(array($sessionid, $count));
  //   $res = $q->fetchAll(PDO::FETCH_COLUMN);
  //   $db = null;
  //   return $res;
  // }

  function clearDealtCards($sessionid) {
    $db = dbConnect();
    $q=$db->prepare("
      DELETE FROM dealtcards WHERE session = ?
    ");
    $q->execute(array($sessionid));
    $db = null;
  }
  
  function dealToPlayer($sessionid, $playerid, $count=1) {
    $db = dbConnect();
    $q=$db->prepare("
      INSERT INTO dealtcards
      (session, placement, player, card)    
      SELECT ? as 'session', ? as 'placement', ? as 'player', deceits.id as 'card'
      FROM deceits
      LEFT JOIN dealtcards ON dealtcards.session = ? AND dealtcards.card = deceits.id
      WHERE dealtcards.card IS NULL
      ORDER BY RANDOM()
      LIMIT ?
    ");
    $q->execute(array($sessionid, CARD_INHAND, $playerid, $sessionid, $count));
    $db = null;
  }

  function getPlayerHand($sessionid, $playerid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT deceits.* FROM dealtcards
      INNER JOIN deceits ON dealtcards.card = deceits.id
      WHERE dealtcards.session=? AND dealtcards.player=? AND dealtcards.placement=?
    ");
    $q->execute(array($sessionid, $playerid, CARD_INHAND));
    $res = $q->fetchAll(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function getCard($cardid) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT * FROM deceits
      WHERE id = ?
    ");
    $q->execute(array($cardid));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function makeHeroChoice($session, $choice) {
    // the choice value to record
    $choiceValue = CHOICE_UNCHOSEN;
    if ($choice == 'A') $choiceValue = CHOICE_OPTIONA;
    if ($choice == 'B') $choiceValue = CHOICE_OPTIONB;

    $db = dbConnect();
    $q=$db->prepare('
      UPDATE sessions
      SET herochoice = ?, state = ?
      WHERE id = ?
    ');
    $q->execute(array($choiceValue, STATE_TRICKSTER_CHOOSING, $session['id']));
    $db = null;
    return true;
  }

  function makeTricksterChoice($session, $cardid) {

    $db = dbConnect();

    // record the choice
    $q=$db->prepare('
      UPDATE sessions
      SET deceit = ?, state = ?
      WHERE id = ?
    ');
    $q->execute(array($cardid, STATE_HERO_RESPONDING, $session['id']));

    // take the card out of the trickster's hand
    $q=$db->prepare('
      UPDATE dealtcards 
      SET placement=?
      WHERE session=? AND card=?
    ');
    $q->execute(array(CARD_PLAYED, $session['id'], $cardid));

    // put a new card in the trickster's hand
    dealToPlayer($session['id'], $session['trickster'], 1);

    $db = null;
    return true;
  }

  function swap(&$x, &$y) {
      $tmp=$x;
      $x=$y;
      $y=$tmp;
  }

  // when a hero responds.
  function makeHeroResponse($session, $choice) {

    // check if the hero changed their mind
    $choiceValue = CHOICE_UNCHOSEN;
    if ($choice == 'A') $choiceValue = CHOICE_OPTIONA;
    if ($choice == 'B') $choiceValue = CHOICE_OPTIONB;
    $herochangedmind = ($session['herochoice'] != $choiceValue);

    // record the outcome
    recordHistory($session, $herochangedmind);

    // choose the next trickster (if there is one)
    $newtrickster = getNextTrickster($session);
    if ($newtrickster == -1) {
      // everybody has had their turn. time for a new round.
      $newhero = getNextHero($session);
      if ($newhero == -1) {
        // echo "TIME FOR A NEW ROUND";

        // pick a new random hero and trickster
        $hero_and_trickster = getRandomPlayerIDs($session['id'], 2);
        $hero = $hero_and_trickster[0];
        $trickster = $hero_and_trickster[1];
        // make sure it's not the same hero again
        if ($hero == $session['hero']) swap($hero, $trickster);

        // pick a new dilemma
        $dilemma = getRandomDilemma();

        $db = dbConnect();
        $q=$db->prepare('
          UPDATE sessions
          SET state = ?, hero = ?, trickster = ?, dilemma = ?, herochoice = ?, round = round + 1, deceit = -1
          WHERE id = ?
        ');
        $q->execute(array(STATE_HERO_CHOOSING, $hero, $trickster, $dilemma, CHOICE_UNCHOSEN, $session['id']));
        $db = null;
        return true;
      } else {
        // update the session with the new hero and state
        $db = dbConnect();
        $q=$db->prepare('
          UPDATE sessions
          SET state = ?, hero = ?, trickster = -1
          WHERE id = ?
        ');
        $q->execute(array(STATE_HERO_CHOOSING, $newhero, $session['id']));
        $db = null;

        // try again to get a new trickster (should work now)
        $session['hero'] = $newhero;
        $newtrickster = getNextTrickster($session);

        // figure out a new dilemma (for now just pure random)
        $dilemma = getRandomDilemma();

        // update the session with the new trickster
        $db = dbConnect();
        $q=$db->prepare('
          UPDATE sessions
          SET trickster = ?, dilemma = ?
          WHERE id = ?
        ');
        $q->execute(array($newtrickster, $dilemma, $session['id']));
        $db = null;
        return true;
      }
    } else {
      // update the session with the new trickster and state
      $db = dbConnect();
      $q=$db->prepare('
        UPDATE sessions
        SET state = ?, trickster = ?, deceit = -1
        WHERE id = ?
      ');
      $q->execute(array(STATE_TRICKSTER_CHOOSING, $newtrickster, $session['id']));
      $db = null;
    }
    return true;
  }

  function getNextTrickster($session) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT players.id FROM players
      LEFT JOIN history ON history.session = players.session AND history.round = ? AND history.hero = ? AND history.trickster = players.id
      WHERE history.session IS NULL AND players.session = ? AND players.id != ?
      ORDER BY RANDOM()
      LIMIT 1
    ");
    $q->execute(array($session['round'], $session['hero'], $session['id'], $session['hero']));;
    $res = $q->fetchAll(PDO::FETCH_COLUMN);
    $db = null;
    if (count($res)==0) return -1;
    return $res[0];
  }

  function getNextHero($session) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT players.id FROM players
      LEFT JOIN history ON history.session = players.session AND history.round = ? AND history.hero = players.id
      WHERE history.session IS NULL AND players.session = ?
      ORDER BY RANDOM()
      LIMIT 1
    ");
    $q->execute(array($session['round'], $session['id']));;
    $res = $q->fetchAll(PDO::FETCH_COLUMN);
    $db = null;
    if (count($res)==0) return -1;
    return $res[0];
  }

  function recordHistory($session, $successful) {
    $db = dbConnect();
    $q=$db->prepare("
      INSERT INTO history
      (session, round, hero, dilemma, trickster, deceit, successful)
      VALUES (?, ?, ?, ?, ?, ?, ?)
    ");
    $q->execute(array(
      $session['id'],
      $session['round'],
      $session['hero'],
      $session['dilemma'],
      $session['trickster'],
      $session['deceit'],
      $successful
    ));
    $db = null;
    return $id;
  }


?>