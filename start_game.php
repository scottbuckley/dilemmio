<?php
  require_once 'db/common.php';

  $sessionid = $_REQUEST['session'];
  $pass      = $_REQUEST['pass'];
  $playerid  = $_REQUEST['player'];

  // get session and check it's all good
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);
  dieIfNotAdmin($session, $playerid);
  $sessionid = $session['id'];

  // start the game
  startGame($sessionid);

  // set a random hero and trickster
  $hero_and_trickster = getRandomPlayerIDs($sessionid, 2);
  setHero($sessionid, $hero_and_trickster[0]);
  setTrickster($sessionid, $hero_and_trickster[1]);

  // set a random dilemma
  $dilemma = getRandomDilemma();
  setDilemma($sessionid, $dilemma);

  // deal cards to everybody
  clearDealtCards($sessionid);
  $players = getPlayersInSession($sessionid);
  foreach ($players as $playerid => $playername)
     dealToPlayer($sessionid, $playerid, 5);

?>
<html>
   <head>
      <title>Starting session</title>
      <meta http-equiv = 'refresh' content = '0; url = .?<?=$_SERVER['QUERY_STRING']?>' />
   </head>
   <body>
      <p>Starting the session...</p>
   </body>
</html>