# Dilemmio
This is all PHP, JS using only jQuery and Bootstrap 4 (CSS only). Dump the files on your 
PHP server and it should just work. If you want to create a fresh database, /db/makedb.php 
will do that. I made some data entry forms for entering 'dilemma' and 'deceit' cards, 
including some basic parsing for the output of OCR text for the dilemmas.

Most of the interesting code is in /db/common.php. /game.php renders most of the game 
interface.

No sockets or anything - pages that aren't waiting for user input do an ajax request at a 
set interval to decide when they should refresh the whole page. Only the waiting room does 
ajax-based page updates.

It's recommended you password protect the /db/ directory. I use .htaccess to do this. The 
database is SQLite. If you want to do any manual stuff with the database, phpliteadmin is 
useful.

## Limitations
Currently there's only random ordering of 'heros' and 'tricksters'. Deceit cards are dealt 
properly (avoiding repeats), but dilemmas are currently chosen randomly, so can certainly 
repeat in a game.

## Logic
This is a rough state diagram for the game logic.
![Dilemmio States](/StateDiagram.png)
Mermaid syntax:
```
stateDiagram
  [*] --> JoinSession
  JoinSession --> WaitingRoom
  JoinSession --> CreateSession
  CreateSession --> WaitingRoom: as admin
  WaitingRoom --> HeroChoice: admin starts game

  HeroChoice --> TricksterChoice: hero chooses option
  TricksterChoice --> HeroResponse: trickster plays deceit
  HeroResponse --> TricksterChoice: hero responds\n(new trickster)
  HeroResponse --> HeroChoice: hero responds\n(new hero)
```

## Excluded Deceits
I didn't include any of the following deceit cards in my database, as they would have required
me to write in their extra functionality.

### Discard
You may choose one deceit card in the deceit pile and use that card instead of this one.

### Free Space
The trickster who plays this card gets a free point. Yeah, sometimes the world isn't fair.

### Binary
01001001 01100110 00100000 01110100 
01101000 01100101 00100000 01101000 
01100101 01110010 01101111 00100000 
01100011 01100001 01101110 00100000 
01110010 01100101 01100001 01100100 
00100000 01110100 01101000 01101001 
01110011 00101100 00100000 01111001 
01101111 01110101 00100000 01100111 
01100101 01110100 00100000 01100001 
00100000 01100110 01110010 01100101 
01100101 00100000 01110000 01101111 
01101001 01101110 01110100 00101110

### Shuffle
The trickster may take the deceit card off the top of the deck and play that one instead of this one. This card then gets discarded.

### Dilemma Shift
*The trickster playin this card goes first* The hero must pick the dilemma card off the top of the deck and play that one instead of the current one. No other tricksters get to change deceit cards. You get a point if no one else does this round.
