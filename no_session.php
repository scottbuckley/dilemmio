<?php
  require_once 'db/common.php';
  $sessions = getWaitingSessions();
  $sessionCount = count($sessions);
?>
<html>
  <head>
    <? htmlHead(); ?>
    <title>Dilemmio</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align:center;">Join a session</h2>
      <form action="join_session.php" method="get">
        <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">Your Name</label>
          <div class="col-sm-10">
            <input type="text" name="name" class="form-control" id="inputName" required autofocus>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputSession" class="col-sm-2 col-form-label">Session</label>
          <div class="col-sm-10">
            <select class="form-control" name="session" id="inputSession" size="<?=$sessionCount?>" required>
            <?php
              foreach($sessions as $session)
                echo "<option value=$session[id]>$session[name]</option>";
            ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPass" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
            <input type="text" name="pass" class="form-control" id="inputPass">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2"></div>
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Join</button>
            <a href="new_session.php" class="btn btn-outline-primary" style="float:right;">Create a new session</a>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>