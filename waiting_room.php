<?php
  require_once 'db/common.php';

  $playerid = $_REQUEST['player'];
  $sessionid = $_REQUEST['session'];
  $pass = $_REQUEST['pass'];

  // make sure we have a valid session
  $session = getSession($sessionid, $pass);
  dieIfInvalidSessionOrPlayer($session, $playerid);

  // get a list of players
  $players = getPlayerNamesInSession($sessionid);
  $sessionname = $session['name'];

  // check if you are the admin
  $isAdmin = isAdmin($session, $_REQUEST['player']);
  $startButton = ($isAdmin && count($players)>1);
?>

<html>
  <head>
    <?php htmlHead(); ?>
    <title>Dilemmio</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align:center;">'<?=$sessionname?>' waiting room</h2>
      <ul class="list-group" id="playerList">
      <?php foreach($players as $playername)
          echo "<li class=\"list-group-item\">$playername</li>"; ?>
      </ul>
      <div id="buttonspot"></div>
    </div>
  <script type="text/javascript">
    // jquery onload
    $(function() {
      // makes a 'start game' button, if it doesn't exist
      function makeButton() {
        if ($("#startbutton").length === 0) {
          $("#buttonspot").html('<a class="btn btn-primary" id="startbutton" href="start_game.php?<?=$_SERVER['QUERY_STRING']?>" role="button" onclick="return confirm(\'Start this game and close the waiting room?\');">Start Game</a>');
        }
      }
      <?php if ($startButton) {
        // make the button immediately if it doesn't already exist
        // (if we have enough players already)
        echo 'makeButton();';
      } ?>

      // regular update of the players in this session.
      function updatePlayers(players) {
        var cont = $("#playerList");
        var html = "";
        for (var i=0; i<players.length; i++) {
          var player = players[i];
          html += "<li class=\"list-group-item\">"+player+"</li>"
        }

        // if we;re admin, perhaps we create the button at this point
        <?php if ($isAdmin) { ?>
          if (players.length > 1)
            makeButton();
        <?php } ?>

        cont.html(html);
      }

      // the session has changed. we need to refresh the page.
      function updateSession(session) {
        if (session.phase !== 0) {
          goToGame();
        }
      }

      function goToGame() {
        window.location.href = "game.php?<?=$_SERVER['QUERY_STRING']?>";
      }

      setInterval(function(){
        console.log("getting session update...");
        $.getJSON( "ajax.php?req=waiting&<?=$_SERVER['QUERY_STRING']?>", function( data ) {
          console.log(data);
          if (data.response==="players") {
            updatePlayers(data.players);
          } else if (data.response==="session") {
            updateSession(data.session);
          }
        });
      }, <?=PING_INTERVAL?>);
    });
  </script>
  </body>
</html>